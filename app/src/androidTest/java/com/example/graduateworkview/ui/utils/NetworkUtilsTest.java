package com.example.graduateworkview.ui.utils;

import org.json.JSONException;
import org.junit.Test;

import static org.junit.Assert.*;

public class NetworkUtilsTest {

    private final String bodyAuthentication = "{\"password\":\"demo123\",\"login\":\"demo@owen.ru\"}";

    @Test
    public void whenCreateBodyAuthenticationTaskWithCorrectDataThenReturnCorrectString()
            throws JSONException {
        String body = NetworkUtils.createBodyAuthenticationTask("demo@owen.ru", "demo123");
        assertEquals(bodyAuthentication, body);
    }

}