package com.example.graduateworkview.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Parameter {
    Integer id;
    String name;
    String value;
    String measurement;
}
