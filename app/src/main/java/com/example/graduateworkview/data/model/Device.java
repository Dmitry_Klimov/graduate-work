package com.example.graduateworkview.data.model;

import androidx.annotation.NonNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Device {
    private Integer id;
    private String name;
    private String identifier;
    private String type;
    private String timeZone;
    private Boolean isFavorite;



    @NonNull
    @Override
    public String toString() {
        return  "id : " + id + '\n' +
                "name : " + name + '\n' +
                "identifier : " + identifier + '\n' +
                "type : " + type + '\n' +
                "timeZone : " + timeZone + '\n' +
                "isFavorite : " + isFavorite;
    }
}
