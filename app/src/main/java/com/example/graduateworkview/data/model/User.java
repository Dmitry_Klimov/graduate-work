package com.example.graduateworkview.data.model;

import androidx.annotation.NonNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    //токен для авторизации
    private String token;
    //имя юзера
    private String name;
    //фамилия юзера
    private String surname;
    //название компании юзера
    private String company_name;
    //доменное имя, к которому относится пользователь
    private String domain_name;
    //имеет ли пользователь роль «Управляющий приборами» для своей компании
    private Boolean canManageDevices;
    //имеет ли пользователь роль «Управляющий командами» для своей компании
    private Boolean canManageCommands;
    //включен ли флаг необходимости SMS–подтверждения компании пользователя
    private Boolean smsConfirmation;
    //статус запроса. 0 - запрос выполнен без ошибок,
    //1 - ошибка авторизации, 2 - почта не подтверждена
    private int error_status;


    @NonNull
    @Override
    public String toString() {
        return
                "token :'" + token + '\n' +
                "name : " + name + '\n' +
                "surname : " + surname + '\n' +
                "company_name : " + company_name + '\n' +
                "domain_name : " + domain_name + '\n' +
                "canManageDevices : " + canManageDevices + '\n' +
                "canManageCommands : " + canManageCommands + '\n' +
                "smsConfirmation : " + smsConfirmation + '\n' +
                "error_status : " + error_status + '\n';
    }
}
