package com.example.graduateworkview.ui.requests;

import android.os.AsyncTask;
import android.util.Log;

import com.example.graduateworkview.ui.utils.NetworkUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static com.example.graduateworkview.ui.utils.NetworkUtils.AUTHORIZATION;
import static com.example.graduateworkview.ui.utils.NetworkUtils.BEARER;

public class IdParametersTask extends AsyncTask<String, Void, String> {
    public static final String TAG = "IdParametersTask";

    @Override
    protected String doInBackground(String... strings) {
        String id = strings[0];
        String token = strings[1];
        String answer;
        URL url;
        HttpURLConnection con = null;
        try {
            url = NetworkUtils.createInfoDeviceUrl(id);
            con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            if (token != null && !token.equals("")) {
                con.setRequestProperty(AUTHORIZATION, BEARER + token);
            } else {
                Log.i(TAG, "Token is empty");
                return null;
            }

            StringBuilder response;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            }
            answer = response.toString();
        } catch (MalformedURLException e) {
            Log.e(TAG, "URL is not created", e);
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            Log.e(TAG, "HttpURLConnection error", e);
            e.printStackTrace();
            return null;
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
        return answer;
    }
}
