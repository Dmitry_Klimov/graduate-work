package com.example.graduateworkview.ui.pin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.graduateworkview.R;

public class CreatePinActivity extends AppCompatActivity {

    private Button buttonContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pin);

        buttonContinue = findViewById(R.id.b_continue);
        buttonContinue.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.graduateworkview.ui.display.MainDisplay");
                        startActivity(intent);
                    }
                }
        );
    }
}
