package com.example.graduateworkview.ui.requests;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static com.example.graduateworkview.ui.utils.NetworkUtils.AUTHORIZATION;
import static com.example.graduateworkview.ui.utils.NetworkUtils.BEARER;
import static com.example.graduateworkview.ui.utils.NetworkUtils.createInfoDevicesUrl;

public class AvailableDevicesTask  extends AsyncTask<String, Void, String> {
    public static final String TAG = "AvailableDevicesTask";

    @Override
    protected String doInBackground(String... strings) {
        String token = strings[0];
        String answer;
        URL url;
        HttpURLConnection con;
        try {
            url = createInfoDevicesUrl();
            con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            if (token != null) {
                con.setRequestProperty(AUTHORIZATION, BEARER + token);
            } else {
                Log.i(TAG, "Token is empty");
                return null;
            }

            StringBuilder response;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            }
            answer = response.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return answer;
    }
}
