package com.example.graduateworkview.ui.utils;

import android.util.Log;

import com.example.graduateworkview.ui.requests.AuthenticationTask;
import com.example.graduateworkview.ui.requests.LastDataDevicesTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

public class NetworkUtils {

    //Хост Owen Cloud Api
    private static final String OWEN_API_BASE_URL = "https://api.owencloud.ru";
    //Метод для авторизации
    private static final String OWEN_AUTHENTICATION = "/v1/auth/open";
    private static final String OWEN_INFO_ABOUT_DEVICES = "/v1/device/index";
    private static final String OWEN_INFO_ABOUT_DEVICE = "/v1/device/";
    private static final String OWEN_GET_LAST_DATA_DEVICE = "/v1/parameters/last-data";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
    public static final String ID = "id";

    public static URL createAuthenticationUrl() throws MalformedURLException {
        //возврат и создание URL-объекта
        return new URL(OWEN_API_BASE_URL + OWEN_AUTHENTICATION);
    }

    public static URL createInfoDevicesUrl() throws MalformedURLException {
        return new URL(OWEN_API_BASE_URL + OWEN_INFO_ABOUT_DEVICES);
    }

    public static URL createInfoDeviceUrl(String id) throws MalformedURLException {
        return new URL(OWEN_API_BASE_URL + OWEN_INFO_ABOUT_DEVICE + id);
    }

    public static URL createLastDataDeviceUrl() throws MalformedURLException {
        return new URL(OWEN_API_BASE_URL + OWEN_GET_LAST_DATA_DEVICE);
    }

    public static String createBodyLastDataDevices(String id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        if (id != null && !id.equals("")) {
            jsonArray.put(id);
            jsonObject.put("ids", jsonArray);
        } else {
            Log.i(LastDataDevicesTask.TAG, "Id is empty");
            return null;
        }
        return jsonObject.toString();
    }

    public static String createBodyAuthenticationTask(String login, String password)
            throws JSONException {
        //создание JSON-объекта
        JSONObject jsonObject = new JSONObject();
        //проверка наличия данных для авторизации
        if (login != null
                && !login.equals("")
                && password != null
                && !password.equals("")) {
            //добавление логина в JSON-объект
            jsonObject.put(LOGIN, login);
            //добавление пароля в JSON-объект
            jsonObject.put(PASSWORD, password);
        } else {
            //выдача сообщения об отсутствии данных для авторизации
            Log.i(AuthenticationTask.TAG, "Login or password is empty");
            //возврат нуля при отсутствии данных для авторизации
            return null;
        }
        //преобразование JSON-объекта в строку и возврат строки
        return jsonObject.toString();
    }
}
