package com.example.graduateworkview.ui.display;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.graduateworkview.R;
import com.example.graduateworkview.data.model.Device;
import com.example.graduateworkview.data.model.Parameter;
import com.example.graduateworkview.data.model.User;
import com.example.graduateworkview.ui.requests.AuthenticationTask;
import com.example.graduateworkview.ui.requests.AvailableDevicesTask;
import com.example.graduateworkview.ui.requests.IdParametersTask;
import com.example.graduateworkview.ui.requests.LastDataDevicesTask;
import com.example.graduateworkview.ui.utils.ParserJson;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class MainDisplay extends AppCompatActivity {
    public static final String TAG = "MainDisplay";

    private User user;
    private ArrayList<Device> devices;
    private ArrayList<Parameter> parameters;

    private Button buttonLogin;
    private Button buttonDevice;
    private TextView textViewTerminal;
    private TextView textViewParametersValue;
    private RecyclerView recyclerViewDevicesList;
    private RecyclerView recyclerViewDeviceParameters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_display);

        buttonLogin = findViewById(R.id.b_login);
        buttonDevice = findViewById(R.id.b_get_devices);
        textViewTerminal = findViewById(R.id.tv_terminal);
        textViewParametersValue = findViewById(R.id.tv_parameters_info);
        recyclerViewDevicesList = findViewById(R.id.rv_devices);
        recyclerViewDeviceParameters = findViewById(R.id.rv_parameters);

        textViewTerminal.setVisibility(View.INVISIBLE);
        recyclerViewDevicesList.setVisibility(View.INVISIBLE);

        recyclerViewDevicesList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManagerDeviceList = new LinearLayoutManager(this);
        recyclerViewDevicesList.setLayoutManager(layoutManagerDeviceList);

        recyclerViewDeviceParameters.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManagerParameters = new LinearLayoutManager(this);
        recyclerViewDeviceParameters.setLayoutManager(layoutManagerParameters);

        buttonLogin.setOnClickListener(
                new View.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(View v) {
                        //создание объекта для асинхронного запроса
                        AuthenticationTask authenticationTask = new AuthenticationTask();
                        //запуск асинхронного процесса
                        authenticationTask.execute("demo@owen.ru", "demo123");
                        //создание ссылки для сохранения ответа
                        String answer = null;
                        try {
                            //получение ответа от асихронного запроса
                            answer = authenticationTask.get();
                            //обработка исключительных ситуаций
                        } catch (ExecutionException | InterruptedException e) {
                            //возикающих при получении ответа
                            Log.e(TAG, "Execution error", e);
                            //запись текта об ошибке
                            textViewTerminal.setText("Error, try again");
                            //отображение текста ошибки
                            showTextViewTerminal();
                        }
                        //проверка наличия ответа
                        if (answer != null && !answer.equals("")) {
                            //обработка обвета
                            user = ParserJson.parseAuthenticationJSON(answer);
                            //проверка создания объкта пользователя
                            textViewTerminal.setText(
                                    //если объект пользователя пуст - запись текта об ошибке
                                    user != null ? user.toString() : "Error, try again"
                            );
                            //отображение текста ошибки
                            showTextViewTerminal();
                        }
                        //если ответ от асихронного запроса пуст
                        else {
                            //запись текта об ошибке
                            textViewTerminal.setText("Error, try again");
                            //отображение текста ошибки
                            showTextViewTerminal();
                        }
                    }
                }
        );

        buttonDevice.setOnClickListener(
                new View.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(View v) {
                        AvailableDevicesTask availableDevicesTask = new AvailableDevicesTask();
                        availableDevicesTask.execute(user.getToken());
                        String answer = null;
                        try {
                            answer = availableDevicesTask.get();
                        } catch (ExecutionException | InterruptedException e) {
                            Log.e(TAG, "Execution error", e);
                            textViewTerminal.setText("Error, try again");
                            showTextViewTerminal();
                        }
                        if (answer != null && !answer.equals("")) {
                            devices = ParserJson.parseDevicesJson(answer);
                            RecyclerView.Adapter mAdapter = new DeviceAdapter(devices);
                            recyclerViewDevicesList.setAdapter(mAdapter);
                            showRecyclerViewDevices();
                        } else {
                            textViewTerminal.setText("Error, try again");
                            showTextViewTerminal();
                        }
                    }
                }
        );
    }

    private String getDeviceParameters(Integer id) throws ExecutionException, InterruptedException {
        IdParametersTask idParametersTask = new IdParametersTask();
        idParametersTask.execute(id.toString(), user.getToken());
        return idParametersTask.get();
    }

    private String getParameters(Integer id) throws ExecutionException, InterruptedException {
        LastDataDevicesTask lastDataDevicesTask = new LastDataDevicesTask();
        lastDataDevicesTask.execute(id.toString(), user.getToken());
        return lastDataDevicesTask.get();
    }


    private void showTextViewTerminal() {
        recyclerViewDevicesList.setVisibility(View.INVISIBLE);
        textViewTerminal.setVisibility(View.VISIBLE);
    }

    private void showRecyclerViewDevices() {
        textViewTerminal.setVisibility(View.INVISIBLE);
        recyclerViewDevicesList.setVisibility(View.VISIBLE);
    }

    public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.MyViewHolder> {
        private ArrayList<Device> devices;

        DeviceAdapter(ArrayList<Device> devices) {
            this.devices = devices;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textViewDeviceId;
            TextView textViewDeviceName;
            TextView textViewDeviceType;

            MyViewHolder(View view) {
                super(view);
                textViewDeviceId = view.findViewById(R.id.tv_device_id);
                textViewDeviceName = view.findViewById(R.id.tv_device_name);
                textViewDeviceType = view.findViewById(R.id.tv_device_type);

                itemView.setOnClickListener(
                        new View.OnClickListener() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onClick(View v) {
                                int position = getAdapterPosition();
                                String answer;
                                try {
                                    answer = getDeviceParameters(devices.get(position).getId());
                                    parameters = ParserJson.parseDeviceJson(answer);
                                } catch (ExecutionException | InterruptedException e) {
                                    e.printStackTrace();
                                    Log.e(TAG, "Execution error", e);
                                    textViewTerminal.setText("Error, try again");
                                    showTextViewTerminal();
                                }
                                if (parameters != null) {
                                    RecyclerView.Adapter mAdapter = new ParameterAdapter(parameters);
                                    recyclerViewDeviceParameters.setAdapter(mAdapter);
                                } else {
                                    Log.i(TAG, "ArrayList<Parameter> parameters is empty.");
                                    textViewTerminal.setText("Error, try again");
                                    showTextViewTerminal();
                                }
                            }
                        }
                );
            }

            void bind(String deviceId, String deviceName, String deviceType) {
                textViewDeviceId.setText(deviceId);
                textViewDeviceName.setText(deviceName);
                textViewDeviceType.setText(deviceType);
            }
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            int layoutId = R.layout.layout_recycler_view_devices;

            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(layoutId, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            Device device = devices.get(position);
            holder.bind(device.getId().toString(), device.getName(), device.getType());
        }

        @Override
        public int getItemCount() {
            return devices.size();
        }
    }


    public class ParameterAdapter extends RecyclerView.Adapter<ParameterAdapter.MyViewHolder> {
        private ArrayList<Parameter> parameters;

        ParameterAdapter(ArrayList<Parameter> parameters) {
            this.parameters = parameters;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textViewParameterId;
            TextView textViewParameterName;

            MyViewHolder(View view) {
                super(view);
                textViewParameterId = view.findViewById(R.id.tv_parameter_id);
                textViewParameterName = view.findViewById(R.id.tv_parameter_name);

                itemView.setOnClickListener(
                        new View.OnClickListener() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onClick(View v) {
                                int position = getAdapterPosition();
                                String answer;
                                try {
                                    answer = getParameters(parameters.get(position).getId());
                                    String value = ParserJson.parseParametersJson(answer);
                                    textViewParametersValue.setText(value);
                                    //start loop
                                } catch (ExecutionException | InterruptedException e) {
                                    e.printStackTrace();
                                    Log.e(TAG, "Execution error", e);
                                    textViewTerminal.setText("Error, try again");
                                    showTextViewTerminal();
                                }
                                if (parameters != null) {
                                    RecyclerView.Adapter mAdapter = new ParameterAdapter(parameters);
                                    recyclerViewDeviceParameters.setAdapter(mAdapter);
                                } else {
                                    Log.i(TAG, "ArrayList<Parameter> parameters is empty.");
                                    textViewTerminal.setText("Error, try again");
                                    showTextViewTerminal();
                                }
                            }
                        }
                );
            }

            @SuppressLint("SetTextI18n")
            void bind(String parameterId, String parameterName) {
                textViewParameterId.setText(parameterId);
                textViewParameterName.setText(parameterName);
            }
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            int layoutId = R.layout.layout_recycler_view_parameters;

            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(layoutId, parent, false);

            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            Parameter parameter = parameters.get(position);
            holder.bind(parameter.getId().toString(), parameter.getName());
        }

        @Override
        public int getItemCount() {
            return parameters.size();
        }
    }
}
