package com.example.graduateworkview.ui.requests;

import android.os.AsyncTask;
import android.util.Log;

import com.example.graduateworkview.ui.utils.NetworkUtils;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static com.example.graduateworkview.ui.utils.NetworkUtils.AUTHORIZATION;
import static com.example.graduateworkview.ui.utils.NetworkUtils.BEARER;
import static com.example.graduateworkview.ui.utils.NetworkUtils.createLastDataDeviceUrl;

public class LastDataDevicesTask extends AsyncTask<String, Void, String> {
    public static final String TAG = "LasDataDevicesTask";

    @Override
    protected String doInBackground(String... strings) {
        String id = strings[0];
        String token = strings[1];
        String answer;
        URL url;
        HttpURLConnection con = null;
        try {
            url = createLastDataDeviceUrl();
            con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            if (token != null && !token.equals("")) {
                con.setRequestProperty(AUTHORIZATION, BEARER + token);
            } else {
                Log.i(TAG, "Token is empty");
                return null;
            }

            String jsonInputString = NetworkUtils.createBodyLastDataDevices(id);

            try (OutputStream os = con.getOutputStream()) {
                byte[] input = new byte[0];
                if (jsonInputString != null) {
                    input = jsonInputString.getBytes(StandardCharsets.UTF_8);
                }
                os.write(input, 0, input.length);
            }

            StringBuilder response;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            }
            answer = response.toString();
        } catch (MalformedURLException e) {
            Log.e(TAG, "URL is not created", e);
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            Log.e(TAG, "HttpURLConnection error", e);
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            Log.e(TAG, "JSON body is not created", e);
            e.printStackTrace();
            return null;
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
        return answer;
    }
}
