package com.example.graduateworkview.ui.requests;

import android.os.AsyncTask;
import android.util.Log;

import com.example.graduateworkview.ui.utils.NetworkUtils;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static com.example.graduateworkview.ui.utils.NetworkUtils.createAuthenticationUrl;

public class AuthenticationTask extends AsyncTask<String, Void, String> {
    public static final String TAG = "AuthenticationTask";

    @Override
    protected String doInBackground(String... strings) {
        //получение логина из параметров метода
        String login = strings[0];
        //получение пароля из параметров метода
        String password = strings[1];
        //создание ссылки для сохранения ответа
        String answer;
        //создание ссылки для работы с URL
        URL url;
        //создание ссылки для работы с HttpURLConnection
        HttpURLConnection con = null;
        try {
            //создание Https запроса
            url = createAuthenticationUrl();
            //открытие соединения
            con = (HttpURLConnection) url.openConnection();
            //настройка запроса
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            //составление тела запроса
            String jsonInputString
                    = NetworkUtils.createBodyAuthenticationTask(login, password);
            //отправка тела запроса
            try (OutputStream os = con.getOutputStream()) {
                byte[] input = new byte[0];
                if (jsonInputString != null) {
                    input = jsonInputString.getBytes(StandardCharsets.UTF_8);
                }
                os.write(input, 0, input.length);
            }
            //получение ответа
            StringBuilder response;
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
            }
            //преобразование ответа в строку
            answer = response.toString();
        //обработка исключительных ситуаций
        } catch (MalformedURLException e) {
            Log.e(TAG, "URL is not created", e);
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            Log.e(TAG, "HttpURLConnection error", e);
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            Log.e(TAG, "JSON body is not created", e);
            e.printStackTrace();
            return null;
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
        return answer;
    }
}
