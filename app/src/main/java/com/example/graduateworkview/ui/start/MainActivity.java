package com.example.graduateworkview.ui.start;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.graduateworkview.R;

public class MainActivity extends AppCompatActivity {

    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        clickButtonLestener();
    }

    public void clickButtonLestener() {
        buttonLogin = findViewById(R.id.buttonLogin);

        buttonLogin.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.graduateworkview.ui.login.LoginActivity");
                        startActivity(intent);
                    }
                }
        );
    }
}
