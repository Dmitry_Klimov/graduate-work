package com.example.graduateworkview.ui.utils;

import android.util.Log;

import com.example.graduateworkview.data.model.Device;
import com.example.graduateworkview.data.model.Parameter;
import com.example.graduateworkview.data.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParserJson {
    private static final String TAG = "ParserJson";

    private static final String TOKEN = "token";
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String COMPANY_NAME = "company_name";
    private static final String DOMAIN_NAME = "domain_name";
    private static final String CAN_MANAGE_DEVICES = "canManageDevices";
    private static final String CAN_MANAGE_COMMANDS = "canManageCommands";
    private static final String SMS_CONFIRMATION = "smsConfirmation";
    private static final String ERROR_STATUS = "error_status";

    private static final String ID = "id";
    private static final String IDENTIFIER = "identifier";
    private static final String TYPE = "type";
    private static final String TIME_ZONE = "time_zone";
    private static final String IS_FAVORITE = "is_favourite";

    private static final String PARAMETERS = "parameters";
    private static final String MEASUREMENT = "measurement";
    private static final String VALUE = "value";
    private static final String TITLE = "title";

    private static final String VALUES = "values";
    private static final String PARAMETER_VALUE = "v";


    public static User parseAuthenticationJSON(String authenticationJSON) {
        //проверка наличия данных для обработки
        if (authenticationJSON != null && !authenticationJSON.equals("")) {
            try {
                //создание JSON-объекта из строки данных
                JSONObject jsonObject = new JSONObject(authenticationJSON);
                //содание и возврат User-объекта
                return new User(
                        //получение токена
                        jsonObject.getString(TOKEN),
                        //получение имени
                        jsonObject.getString(NAME),
                        //получение фамилии
                        jsonObject.getString(SURNAME),
                        //получение названия компании
                        jsonObject.getString(COMPANY_NAME),
                        //получение доменного имени
                        jsonObject.getString(DOMAIN_NAME),
                        //получение роли «Управляющий приборами»
                        jsonObject.getBoolean(CAN_MANAGE_DEVICES),
                        //получение роли «Управляющий командами»
                        jsonObject.getBoolean(CAN_MANAGE_COMMANDS),
                        //получение флага необходимости SMS–подтверждения
                        jsonObject.getBoolean(SMS_CONFIRMATION),
                        //получение статуса запроса
                        jsonObject.getInt(ERROR_STATUS)
                );
            //обработка исключительной ситуациии
            } catch (JSONException e) {
                //сообщение об ошибке
                Log.e(TAG, "Parse failed", e);
                e.printStackTrace();
                //возврат нуля
                return null;
            }
        //если нет данных для обработки
        } else {
            //сообщение об отсутствии данных
            Log.i(TAG, "JsonString is empty");
            //возврат нуля
            return null;
        }
    }

    public static ArrayList<Device> parseDevicesJson(String devicesJson) {
        if (devicesJson != null && !devicesJson.equals("")) {
            try {
                ArrayList<Device> devices = new ArrayList<>();
                JSONArray jsonArray = new JSONArray(devicesJson);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject tempJsonObject = jsonArray.getJSONObject(i);
                    Device device = new Device(
                            tempJsonObject.getInt(ID),
                            tempJsonObject.getString(NAME),
                            tempJsonObject.getString(IDENTIFIER),
                            tempJsonObject.getString(TYPE),
                            tempJsonObject.getString(TIME_ZONE),
                            tempJsonObject.getBoolean(IS_FAVORITE)
                    );
                    devices.add(device);
                }
                return devices;
            } catch (JSONException e) {
                Log.e(TAG, "Parse failed", e);
                e.printStackTrace();
                return null;
            }
        } else {
            Log.i(TAG, "JsonString is empty");
            return null;
        }
    }

    public static ArrayList<Parameter> parseDeviceJson(String deviceJson) {
        if (deviceJson != null && !deviceJson.equals("")) {
            try {
                ArrayList<Parameter> parameters = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(deviceJson);
                JSONArray jsonArray = jsonObject.getJSONArray(PARAMETERS);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject tempJsonObject = jsonArray.getJSONObject(i);
                    JSONObject measurement = tempJsonObject.getJSONObject(MEASUREMENT);
                    Parameter parameter = new Parameter(
                            tempJsonObject.getInt(ID),
                            tempJsonObject.getString(NAME),
                            tempJsonObject.getString(VALUE),
                            measurement.getString(TITLE)
                    );
                    parameters.add(parameter);
                }
                return parameters;
            } catch (JSONException e) {
                Log.e(TAG, "Parse failed", e);
                e.printStackTrace();
                return null;
            }
        } else {
            Log.i(TAG, "JsonString is empty");
            return null;
        }
    }

    public static String parseParametersJson(String parametersJson) {
        if (parametersJson != null && !parametersJson.equals("")) {
            try {
                JSONArray jsonArray = new JSONArray(parametersJson);
                JSONObject jsonObjectInArray = jsonArray.getJSONObject(0);
                JSONArray jsonArrayValues = jsonObjectInArray.getJSONArray(VALUES);
                JSONObject jsonObject = jsonArrayValues.getJSONObject(0);
                return jsonObject.getString(PARAMETER_VALUE);
            } catch (JSONException e) {
                Log.e(TAG, "Parse failed", e);
                e.printStackTrace();
                return null;
            }
        } else {
            Log.i(TAG, "JsonString is empty");
            return null;
        }
    }
}
