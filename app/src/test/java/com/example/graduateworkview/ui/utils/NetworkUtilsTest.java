package com.example.graduateworkview.ui.utils;

import org.json.JSONException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class NetworkUtilsTest {

    @Test
    public void whenCreateBodyAuthenticationTaskWithCorrectDataThenReturnCorrectString()
            throws JSONException {
        //Построение тела запроса с корректными данными
        String body = NetworkUtils.createBodyAuthenticationTask(
                "demo@owen.ru", "demo123");
        //Сравнивание готового тела с заранее известным шаблоном
        String bodyAuthentication = "{\"password\":\"demo123\",\"login\":\"demo@owen.ru\"}";
        assertEquals(bodyAuthentication, body);
    }

    @Test
    public void whenCreateBodyAuthenticationTaskWithIncorrectDataThenReturnNull()
            throws JSONException {
        //Построение тела запроса с некорректными данными
        String body = NetworkUtils.createBodyAuthenticationTask(
                "", "");
        //Проверка на равенство Null
        assertNull(body);
    }

    @Test
    public void whenCreateBodyAuthenticationTaskWithIncorrectLoginThenReturnNull()
            throws JSONException {
        //Построение тела запроса с некорректным логином
        String body = NetworkUtils.createBodyAuthenticationTask(
                "", "demo123");
        //Проверка на равенство Null
        assertNull(body);
    }

    @Test
    public void whenCreateBodyAuthenticationTaskWithIncorrectPasswordThenReturnNull()
            throws JSONException {
        //Построение тела запроса с некорректным паролем
        String body = NetworkUtils.createBodyAuthenticationTask(
                "demo@owen.ru", "");
        //Проверка на равенство Null
        assertNull(body);
    }

    @Test
    public void whenCreateBodyAuthenticationTaskWithNullDataThenReturnNull()
            throws JSONException {
        //Построение тела запроса с нулевыми данными
        String body = NetworkUtils.createBodyAuthenticationTask(
                null, null);
        //Проверка на равенство Null
        assertNull(body);
    }

    @Test
    public void whenCreateBodyAuthenticationTaskWithNullLoginThenReturnNull()
            throws JSONException {
        //Построение тела запроса с нулевым логином
        String body = NetworkUtils.createBodyAuthenticationTask(
                null, "demo123");
        //Проверка на равенство Null
        assertNull(body);
    }

    @Test
    public void whenCreateBodyAuthenticationTaskWithNullPasswordThenReturnNull()
            throws JSONException {
        //Построение тела запроса с нулевым паролем
        String body = NetworkUtils.createBodyAuthenticationTask(
                "demo@owen.ru", null);
        //Проверка на равенство Null
        assertNull(body);
    }
}